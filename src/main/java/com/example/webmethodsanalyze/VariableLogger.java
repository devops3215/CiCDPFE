package com.example.webmethodsanalyze;

import java.util.List;
import java.util.logging.Logger;

public class VariableLogger {

    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

    public static void logVariables(PackageNode node) {

        if (node.getName().endsWith(".xml") && node.getxmlContent() != null) {

            List<String> variableNames = VariablesAndMethods.verifyVariableNames(node.getxmlContent());
            for (String variableName : variableNames) {
                String validationMessage = NamingConventionVerifier.verifyVariableName(variableName);
                if (!validationMessage.startsWith("Valid")) {
                    logger.warning("Invalid variable: " + variableName + " - " + validationMessage + " --serviceName-- : " + node.getParent().getName());
                }else if (validationMessage.startsWith("Valid")) {
                    logger.info("variable: " + variableName + " - " + validationMessage + " --serviceName-- : " + node.getParent().getName());
                }
            }
        }

        for (PackageNode child : node.getChildren()) {
            logVariables(child);
        }
    }
}
