package com.example.webmethodsanalyze;

public class VariablesException extends Exception {
	
	public VariablesException() {
        super();
    }

    
    public VariablesException(String message) {
        super(message);
    }
}
