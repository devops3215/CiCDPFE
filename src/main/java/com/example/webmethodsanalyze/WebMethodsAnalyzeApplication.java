package com.example.webmethodsanalyze;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

@SpringBootApplication
public class WebMethodsAnalyzeApplication {

    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

    static {
        try {
            // Create a file handler to log messages to a file
            FileHandler fileHandler = new FileHandler("app.log", true);
            fileHandler.setFormatter(new SimpleFormatter());

            // Create a console handler to display logs in the console
            ConsoleHandler consoleHandler = new ConsoleHandler();

            // Add handlers to the logger
            logger.addHandler(fileHandler);
            logger.addHandler(consoleHandler);

            // Set log levels
            logger.setLevel(Level.ALL);
            fileHandler.setLevel(Level.ALL);
            consoleHandler.setLevel(Level.ALL);

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to initialize file handler for logger", e);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(WebMethodsAnalyzeApplication.class, args);
    	/* if (args.length > 0 && args[0].equals("methodOne")) {
             methodOne(args[1]);
         } else if (args.length > 0 && args[0].equals("methodTwo")) {
             methodTwo(null, null);
         }*/
    }
    
    public static void methodOne(String param) {
		System.out.println("------------------tst--------");
		System.out.println(param);
    }
    
    
    @Bean
    public CommandLineRunner runMethods( MyService myService , ApplicationContext ctx ) {
    	System.out.println("--------tdt---------");
        return args -> {
            if (args.length > 0) {
                switch (args[0]) {
                    case "CheckVariables":
                        if (args.length > 1) {
                            myService.CheckVariables(ctx,args[1],args[2]);
                        } else {
                            System.out.println("methodOne requires 1 parameter");
                        }
                        break;
                    case "PropertyCheck" :
                    	if (args.length > 1) {
                            myService.PropertyCheck(ctx,args[1],args[2]);
                        } else {
                            System.out.println("methodOne requires 1 parameter");
                        }
                        break;
                    case "CodeStructure":
                        if (args.length > 1) {
                            myService.CodeStructure(ctx,args[1], args[2]);
                        } else {
                            System.out.println("methodOne requires 1 parameter");
                        }
                        break;
                    case "InvokedStagesCheck":
                        if (args.length > 1) {
                            myService.InvokedStagesCheck(ctx,args[1],args[2]);
                        } else {
                            System.out.println("methodOne requires 1 parameter");
                        }
                        break;    
                    case "CheckNamingRules":
                        if (args.length > 1) {
                            myService.CheckNamingRules(ctx,args[1],args[2]);
                        } else {
                            System.out.println("methodOne requires 1 parameter");
                        }
                        break;
                    case "getPackageNodeAndSerialiseIT":
                        if (args.length > 1) {
                            myService.getPackageNodeAndSerialiseIT(ctx,args[1]);
                        } else {
                            System.out.println("methodTwo requires 2 parameters");
                        }
                        break;
                    default:
                        System.out.println("Unknown method");
                }
            } else {
                System.out.println("No method specified");
            }
        };
    
}

@Service
class MyService {
    public void methodOne(ApplicationContext ctx,String param){
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            System.out.println("Résultat de methodOne: " + root.getName());
            logger.log(Level.INFO, "SerialiseIT");
           
            
            SpringApplication.exit(ctx);
            
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);    
        	SpringApplication.exit(ctx);
        	}
        
    }
    public void CheckNamingRules(ApplicationContext ctx, String param, String param2) {
    	try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            logger.log(Level.INFO, "SerialiseIT");
            //method
            switch (param2) {
                case "PACKAGE":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "RootFolder":
                    if (root.getParent() == null) {
                        PackageStructureLogger.logStructure(root, "FOLDER");
                    } else {
                        logger.severe("Not a root folder: {}" + root.getName());
                    }
                    break;
                case "FOLDER":
                    PackageStructureLogger.logStructure(root, param2);
                    break;

                case "FLOW":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "JAVA_SERVICE":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "TRIGGER":
                    PackageStructureLogger.logStructure(root, param2);
                        break;
                case "ADAPTER_SERVICE":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "CONNECTION_DATA":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "DOCUMENT_TYPE":
                    PackageStructureLogger.logStructure(root, param2);
                    break;
                case "Varibles":
                    VariableLogger.logVariables(root);
                     break;
                default:
                System.out.println("Unknown method");
        }

            SpringApplication.exit(ctx);
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
        	}


	}
	public void CheckVariables (ApplicationContext ctx, String param, String param2) throws VariablesException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            System.out.println("Résultat de Performance: " + root.getName());
            logger.log(Level.INFO, "SerialiseIT for Performance Variables check");
            FileCollector files=new FileCollector();
            files.collectFiles(root);
            List<PackageNode> xmlFiles = new ArrayList<>(files.getXmlFiles());
            List<PackageNode> ndfFiles = new ArrayList<>(files.getNdfFiles());
            switch (param2) {
            case "CheckUnusedVariable":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error=0;
            	for (PackageNode xmlFile : xmlFiles) {
            		if(xmlFile.getxmlContent()!=null) {
                		if(VariablesCheck.checkUnusedVariable(xmlFile.getxmlContent(), xmlFile.getParent().getName())) Error++;
            		}
            	}
            	//logger.severe("The package contains "+Error+" Errors interms of unused variables");
            	if(Error>0) throw new VariablesException("The package contains "+Error+" Errors in terms of flow services with unused variables");
            
                break;
                
            case "checkOutputInputSpecified":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error1=0;
            	for (PackageNode ndfFile : ndfFiles) {
            		System.out.println("1------------------------------");
            		if(ndfFile.getxmlContent()!=null) {
            			System.out.println("1------------------------------"+ ndfFile.getParent().getName());
                		if(!InputOutputCheck.checkOutputInputSpecified(ndfFile.getxmlContent(), ndfFile.getParent().getName())) Error1++;

            		}
            	}
            	//logger.severe("The package contains "+Error+" Errors interms of unused variables");
            	if(Error1>0) throw new VariablesException("The package contains "+Error1+" Errors in terms of Specifying Output and Input variables");
            
                break;
                
            case "checkInputOutputValidator":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error2=0;
            	for (PackageNode ndfFile : ndfFiles) {
            		System.out.println("1------------------------------");
            		if(ndfFile.getxmlContent()!=null) {
            			System.out.println("1------------------------------"+ ndfFile.getParent().getName());
                		if(!InputOutputCheck.checkInputOutputValidator(ndfFile.getxmlContent(), ndfFile.getParent().getName())) Error2++;

            		}
            	}
            	if(Error2>0) logger.warning("The package contains "+Error2+" Errors interms of Validationg Input and Output variables");
            
                break;
            case "checkoutPutVariables":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error3=0;
            	for ( int i=0; i< ndfFiles.size();i++) {
            		if(ndfFiles.get(i)!=null&&xmlFiles.get(i)!=null) {
            			if(ndfFiles.get(i).getParent().getName().equals(xmlFiles.get(i).getParent().getName())) {
                    		if(InputOutputCheck.checkoutPutVariables(xmlFiles.get(i).getxmlContent(),ndfFiles.get(i).getxmlContent(), ndfFiles.get(i).getParent().getName())) Error3++;
            			}
            		}
            	}
            	if(Error3>0) throw new VariablesException("The package contains "+Error3+" Errors interms of Validationg Input and Output variables");
            
                break;    
            
            default:
                System.out.println("Unknown method");
        }
            SpringApplication.exit(ctx);
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
            SpringApplication.exit(ctx);
        }
    }
    
    public void PropertyCheck (ApplicationContext ctx, String param, String param2) throws VariablesException, PropertiesException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            System.out.println("Résultat de Performance: " + root.getName());
            logger.log(Level.INFO, "SerialiseIT for Performance Property check");
            FileCollector files=new FileCollector();
            files.collectFiles(root);
            List<PackageNode> xmlFiles = new ArrayList<>(files.getXmlFiles());
            List<PackageNode> ndfFiles = new ArrayList<>(files.getNdfFiles());
            switch (param2) {
            case "checkStatelessProperty":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error=0;
            	for (PackageNode ndfFile : ndfFiles) {
            		if(ndfFile.getxmlContent()!=null) {
                		if(!PropertiesCheck.checkStatelessProperties(ndfFile.getxmlContent(), ndfFile.getParent().getName())) Error++;

            		}
            	}
            	if(Error>0) logger.warning("The package contains "+Error+" Errors interms of using Stateless Property");
            	else {
            		logger.info("checkStatelessProperty: Valid Check");
            	}
                break;
                
            case "checkCachingProperty":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error1=0;
            	for ( int i=0; i< ndfFiles.size();i++) {
            		if(ndfFiles.get(i)!=null&&xmlFiles.get(i)!=null) {
            			if(ndfFiles.get(i).getParent().getName().equals(xmlFiles.get(i).getParent().getName())) {
                    		if(PropertiesCheck.checkCachingProperties(ndfFiles.get(i).getxmlContent(),xmlFiles.get(i).getxmlContent(), ndfFiles.get(i).getParent().getName())) Error1++;
            			}
            		}
            	}
            	//logger.severe("The package contains "+Error+" Errors interms of unused variables");
            	if(Error1>0) throw new PropertiesException("The package contains "+Error1+" Errors (per flow) in terms of Checking Caching Properties");
            	else {
            		logger.info("checkCachingProperty: Valid Check");
            	}
                break;
                
            case "checkAuditProperties":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error2=0;
            	for (PackageNode ndfFile : ndfFiles) {
            		if(ndfFile.getxmlContent()!=null) {
                		if(PropertiesCheck.checkAuditProperties(ndfFile.getxmlContent(), ndfFile.getParent().getName())) Error2++;

            		}
            	}
            	if(Error2>0) throw new PropertiesException("The package contains "+Error2+" Errors (per flow) in terms of Checking Auditing Properties");
            	else {
            		logger.info("checkAuditProperties: Valid Check");
            	}
                break;
            case "checkDisableProperty":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error3=0;
            	for (PackageNode xmlFile : xmlFiles) {
            		if(xmlFile.getxmlContent()!=null) {
                		if(PropertiesCheck.checkDisableProperty(xmlFile.getxmlContent(), xmlFile.getParent().getName())) Error3++;

            		}
            	}
            	if(Error3>0) throw new PropertiesException("The package contains "+Error3+" Errors (per flow) in terms of Checking the Disable Property");
            	else {
            		logger.info("checkDisableProperty: Valid Check");
            	}
                break;    
            
            default:
                System.out.println("Unknown method");
        }
            SpringApplication.exit(ctx);
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
            SpringApplication.exit(ctx);
        }
    }
    
    
    public void InvokedStagesCheck (ApplicationContext ctx, String param, String param2) throws VariablesException, PropertiesException, InvokedStagesCheckException {
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            System.out.println("Résultat de Performance: " + root.getName());
            logger.log(Level.INFO, "SerialiseIT for Performance InvokedStages check");
            FileCollector files=new FileCollector();
            files.collectFiles(root);
            List<PackageNode> xmlFiles = new ArrayList<>(files.getXmlFiles());
            List<PackageNode> ndfFiles = new ArrayList<>(files.getNdfFiles());
            switch (param2) {
            case "CheckInvokedServices":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error=0;
            	for (PackageNode xmlFile : xmlFiles) {
            		if(xmlFile.getxmlContent()!=null) {
                		if(InvokedServicesCheck.CheckInvokedServices(xmlFile.getxmlContent(), xmlFile.getParent().getName())) Error++;

            		}
            	}
            	//logger.severe("The package contains "+Error+" Errors interms of unused variables");
            	if(Error>0) throw new InvokedStagesCheckException("The package contains "+Error+" Errors (per flow) in terms of Invoked Services");
            	else {
            		logger.info("CheckInvokedServices: Valid Check");
            	}
                break;
                
            case "LoopCheck":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error1=0;
            	for (PackageNode xmlFile : xmlFiles) {
            		if(xmlFile.getxmlContent()!=null) {
                		if(LoopCheck.CheckLoops(xmlFile.getxmlContent(), xmlFile.getParent().getName())) Error1++;

            		}
            	}
            	//logger.severe("The package contains "+Error+" Errors interms of unused variables");
            	if(Error1>0) throw new InvokedStagesCheckException("The package contains "+Error1+" Errors (per flow) in terms of Invoking Services in Loop");
            	else {
            		logger.info("LoopCheck: Valid Check");
            	}
                break;
                
            case "CheckComments":
            	System.out.println("-------------------------------------------REALMADRID---------------------------------------");
            	int Error2=0;
            	for (PackageNode xmlFile : xmlFiles) {
            		if(xmlFile.getxmlContent()!=null) {
                		if(StepsCommentsCheck.checkCommentsNeeded(xmlFile.getxmlContent(), xmlFile.getParent().getName())) Error2++;

            		}
            	}
            	if(Error2>0) logger.warning("The package contains "+Error2+" Errors interms number of comments");
            	else {
            		logger.info("CheckComments: Valid Check");
            	}
                break;
             
            
            default:
                System.out.println("Unknown method");
        }
            SpringApplication.exit(ctx);
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
            SpringApplication.exit(ctx);
        }
    }
    
    
    
    
    public void CodeStructure (ApplicationContext ctx,String param, String param2){
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
            PackageStructureVerifier verifier = ctx.getBean(PackageStructureVerifier.class);
            boolean isConnection = ctx.getEnvironment().getProperty("connection.package", Boolean.class, false);
            boolean isAdapter = ctx.getEnvironment().getProperty("adapter.package", Boolean.class, false);
            switch (param2)
            {
            case "checkRootFolder":
                verifier.verifyRootFolder(root);
                break;
            case "checkObligatoryFolders":
                verifier.verifyObligatoryFolders(root,isConnection, isAdapter );
                break;
                default:
                System.out.println("Unknown method");
        }
            SpringApplication.exit(ctx);


        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
            SpringApplication.exit(ctx);
        }
    }
    
   /*
    public void CheckSequenceStructure (ApplicationContext ctx,String param){
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(param))) {
        	PackageNode root = (PackageNode) in.readObject();
si=
            SpringApplication.exit(ctx);
        } catch (IOException | ClassNotFoundException e) {
        	logger.log(Level.SEVERE, "Exception occurred", e);
        	SpringApplication.exit(ctx);
        }
    }
    */
    public void methodTwo(String param) {
        System.out.println("Method One Executed with param: " + param);
        
    }

    public void getPackageNodeAndSerialiseIT(ApplicationContext ctx, String param) {
    	PackageStructureVerifier verifier = ctx.getBean(PackageStructureVerifier.class);
        FileCollector fileCollector = new FileCollector();
        PackageStructurePrinter printer = new PackageStructurePrinter();
        VariableLogger variableLogger = new VariableLogger();
        PackageStructureLogger packageStructureLogger = new PackageStructureLogger();



        File zipFile = new File(param);

        try {
    
            PackageNode root = verifier.getParsedPackageNode(zipFile);
            System.out.println(" ---------------------------------------------------- " + param);
            System.out.println(root.getName());
            System.out.println(" ---------------------------------------------------- " + param);
            logger.log(Level.INFO, "getPackageNodeAndSerialiseIT");
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("result.ser"))) {
                out.writeObject(root);
            }
            System.out.println(" ---------------------------------------------------- " + param);
            
            SpringApplication.exit(ctx);

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Exception occurred", e);
            SpringApplication.exit(ctx);
        }
    }
}

	
    
    
}
