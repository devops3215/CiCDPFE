package com.example.webmethodsanalyze;

public class InvokedStagesCheckException extends Exception {
	
	public InvokedStagesCheckException() {
        super();
    }

    
    public InvokedStagesCheckException(String message) {
        super(message);
    }
}
