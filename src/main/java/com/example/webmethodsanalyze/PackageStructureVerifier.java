package com.example.webmethodsanalyze;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

@Component
public class PackageStructureVerifier {

    private static final Logger logger = Logger.getLogger(PackageStructureVerifier.class.getName());

    private final PackageStructureParser parser;

    @Autowired
    public PackageStructureVerifier(PackageStructureParser parser) {
        this.parser = parser;
    }

    public void verifyPackage(File zipFile) throws IOException {
        PackageNode root = parser.parsePackage(zipFile);
        verifyRootFolder(root);
        verifyObligatoryFolders(root, parser.isConnectionPackage(), parser.isAdapterPackage());
    }

    public PackageNode getParsedPackageNode(File zipFile) throws IOException {
        return parser.parsePackage(zipFile);
    }

    public void verifyRootFolder(PackageNode root) throws IOException {
        long rootFolderCount = root.getChildren().stream()
                .filter(node -> node.getType() == NodeType.FOLDER)
                .count();

        if (rootFolderCount == 0 || rootFolderCount > 1) {
            logger.severe("Root folder is missing or there are multiple root folders in the package.");
        }
    }

    public void verifyObligatoryFolders(PackageNode root, boolean isConnectionPackage, boolean isAdapterPackage) {
        if (root.getType() == NodeType.PACKAGE) {
            PackageNode rootFolder = root.getChildren().stream()
                    .filter(node -> node.getType() == NodeType.FOLDER)
                    .findFirst()
                    .orElse(null);

            if (rootFolder != null) {
                if (isConnectionPackage && isAdapterPackage) {
                    logger.warning("This is a connection package, so you should put adapters in a dedicated package.");
                }
                if (isConnectionPackage) {
                    logger.info("Verifying connection package folders.");
                    verifyConnectionPackageFolders(rootFolder);
                } else if (isAdapterPackage) {
                    logger.info("Verifying adapter package folders.");
                    verifyAdapterPackageFolders(rootFolder);
                } else {
                    logger.info("Verifying non-connection, non-adapter package folders.");
                    checkAndLogFolder(rootFolder, "flows", NodeType.FLOW);
                    checkAndLogFolder(rootFolder, "maps", NodeType.FLOW);
                    checkAndLogFolder(rootFolder, "triggers", NodeType.TRIGGER);
                    checkAndLogFolder(rootFolder, "docs", NodeType.DOCUMENT_TYPE);
                    checkAndLogFolder(rootFolder, "utils", NodeType.FLOW);
                }
            } else {
                logger.warning("Root folder not found.");
            }
        }
    }

    private void verifyConnectionPackageFolders(PackageNode rootFolder) {
        boolean hasConnectionData = rootFolder.getChildren().stream()
                .anyMatch(node -> node.getType() == NodeType.CONNECTION_DATA);
        if (!hasConnectionData) {
            logger.warning("This is a connection package, so it should have only connection data.");
        }

        rootFolder.getChildren().forEach(folder -> {
            if (folder.getType() != NodeType.CONNECTION_DATA && folder.getType() != NodeType.FILE) {
                logger.warning("Folder \"" + folder.getName() + "\" should be removed because this is a connection package and only connection data should be present without any subfolder.");
            }
        });
    }

    private void verifyAdapterPackageFolders(PackageNode rootFolder) {
        checkAndLogFolder(rootFolder, "adapters", NodeType.ADAPTER_SERVICE);
        checkAndLogFolder(rootFolder, "docs", NodeType.DOCUMENT_TYPE);
        checkAndLogFolder(rootFolder, "triggers", NodeType.TRIGGER);
        checkAndLogFolder(rootFolder, "utils", NodeType.FLOW);

        rootFolder.getChildren().forEach(folder -> {
            if (!folder.getName().equals("adapters") &&
                    !folder.getName().equals("docs") &&
                    !folder.getName().equals("triggers") &&
                    !folder.getName().equals("utils")) {
                logger.warning("Folder \"" + folder.getName() + "\" should be removed because this is an adapter package and it should contain only the specified folders.");
            }
        });
    }

    private void checkAndLogFolder(PackageNode parent, String folderName, NodeType expectedType) {
        PackageNode folder = parent.getChildren().stream()
                .filter(node -> node.getName().equals(folderName) && node.getType() == NodeType.FOLDER)
                .findFirst()
                .orElse(null);

        if (folder == null) {
            logger.warning("Missing obligatory folder: " + folderName);
            folder = new PackageNode(folderName, NodeType.FOLDER);
        }

        verifyFolderContents(folder, expectedType);
    }

    private void verifyFolderContents(PackageNode folder, NodeType expectedType) {
        boolean hasValidContent = folder.getChildren().stream()
                .allMatch(node -> node.getType() == expectedType || node.getType() == NodeType.FILE || (expectedType == NodeType.DOCUMENT_TYPE && node.getType() == NodeType.FOLDER));

        if (!hasValidContent) {
            logger.warning("Folder " + folder.getName() + " contains invalid types. Expected: " + expectedType + ".");
        } else if (expectedType == NodeType.DOCUMENT_TYPE) {
            verifyDocumentTypeFolder(folder);
        }
    }

    private void verifyDocumentTypeFolder(PackageNode folder) {
        for (PackageNode child : folder.getChildren()) {
            if (child.getType() == NodeType.FOLDER) {
                boolean subfolderIsEmpty = child.getChildren().stream()
                        .allMatch(node -> node.getType() == NodeType.FILE);
                boolean subfolderHasValidContent = child.getChildren().stream()
                        .allMatch(node -> node.getType() == NodeType.DOCUMENT_TYPE || node.getType() == NodeType.FILE);

                if (!subfolderHasValidContent) {
                    logger.warning("Subfolder " + child.getName() + " in " + folder.getName() + " contains invalid types. Expected: DOCUMENT_TYPE");
                }

                if (subfolderIsEmpty) {
                    logger.warning("Subfolder " + child.getName() + " in " + folder.getName() + " is empty and should be removed");
                }
            }
        }
    }
}
