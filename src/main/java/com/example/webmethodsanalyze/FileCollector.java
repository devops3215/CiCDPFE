package com.example.webmethodsanalyze;

import java.util.ArrayList;
import java.util.List;

public class FileCollector {

    private final List<PackageNode> xmlFiles = new ArrayList<>();
    private final List<PackageNode> ndfFiles = new ArrayList<>();

    public void collectFiles(PackageNode node) {

        if (node.getType() == NodeType.FILE) {

            PackageNode parent = node.getParent();
            if (parent != null && parent.getType() == NodeType.FLOW) {
                if (node.getName().endsWith(".xml")) {
                    xmlFiles.add(node);
                } else if (node.getName().endsWith(".ndf")) {
                    ndfFiles.add(node);
                }
            }
        }

        for (PackageNode child : node.getChildren()) {
            collectFiles(child);
        }
    }

    public List<PackageNode> getXmlFiles() {
        return xmlFiles;
    }

    public List<PackageNode> getNdfFiles() {
        return ndfFiles;
    }
}
