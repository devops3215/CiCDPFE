package com.example.webmethodsanalyze;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class InvokedServicesCheck {
	
    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());
    static Boolean Error=false;
    
    public static Boolean CheckInvokedServices(Node node, String fileName) {
    	traverseAndInvokedServices(node, fileName);
    	return Error;
    }
	
	public static void traverseAndInvokedServices(Node node, String fileName) {
		List<String> servicesPrudenceExplications = new ArrayList<String>() {{
			add("Test : Utiliser avec parcimonie. Peut générer des volumes importants de journaux et affecter les performances.");
		    add("Utiliser avec prudence pour éviter de supprimer des données nécessaires accidentellement.");
		    add("Assurez-vous de manipuler correctement les réponses pour éviter les fuites d'informations sensibles.");
		    add("Assurez-vous de manipuler correctement les réponses pour éviter les fuites d'informations sensibles.");
		    add("Utiliser correctement pour manipuler les codes de réponse HTTP.");
		    add("Assurez-vous de ne pas exposer d'en-têtes sensibles.");
		    add("Similaire à setResponseHeader, à utiliser prudemment.");
		    add("Synchronisation potentiellement perturbatrice.");
		    add("Vérifier les impacts sur les performances.");
		    add("Utiliser pour la maintenance ou les corrections.");
		    add("Utiliser pendant les fenêtres de maintenance.");
		    add("Supprimer des références croisées peut être risqué si cela affecte des flux de travail critiques.");
		    add("Même risque que deleteByObjectId, à utiliser avec précaution.");
		    add("Vérifiez que les variables sont correctement gérées pour éviter les problèmes de sécurité.");
		    add("Utiliser avec prudence pour éviter les interruptions de service.");
		    add("Utiliser avec extrême prudence pour éviter la perte de données critiques.");
		    add("Utiliser avec prudence pour éviter les problèmes de synchronisation.");
		    add("Utiliser avec prudence pour éviter des suppressions accidentelles.");
		    add("Fermer un store en production peut entraîner des problèmes si d'autres processus y accèdent.");
		    add("Supprimer un store peut entraîner une perte de données critique.");
		    add("Libérer tous les verrous peut causer des problèmes de synchronisation si mal géré.");
		    add("Utiliser avec prudence pour éviter des suppressions accidentelles de données critiques.");
		    add("Utiliser avec prudence pour éviter de supprimer des données nécessaires.");
		    add("Utiliser avec prudence pour éviter de supprimer des en-têtes nécessaires.");
		    add("Utiliser avec prudence pour éviter de supprimer des propriétés nécessaires.");
		    add("Utiliser avec prudence pour éviter de désenregistrer des consommateurs actifs.");
		    add("Utiliser avec prudence pour éviter de désenregistrer des fournisseurs actifs.");
		    add("Synchronisation potentiellement perturbatrice.");
		    add("Ce service ferme une séquence WS-ReliableMessaging. Son utilisation doit être soigneusement planifiée car cela peut interrompre le flux de messages.");
		    add("Manipule des mots de passe. Utilisez-les avec précaution pour éviter les fuites de sécurité.");
		    add("Manipule des mots de passe. Utilisez-les avec précaution pour éviter les fuites de sécurité.");
		    add("Manipule des mots de passe. Utilisez-les avec précaution pour éviter les fuites de sécurité.");
		    add("Manipule des mots de passe. Utilisez-les avec précaution pour éviter les fuites de sécurité.");
		    add("Manipule des mots de passe. Utilisez-les avec précaution pour éviter les fuites de sécurité.");
		    add("Peut être utilisé pour effacer le cache d'authentification, mais cela peut entraîner des interruptions de service.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Manipule des données sensibles, utilisez-les avec prudence pour éviter des problèmes de sécurité.");
		    add("Génère un événement de réplication. Utilisez-le avec prudence car il peut déclencher des actions dans d'autres systèmes.");
		    add("Notifie la sortie d'un package. Utilisez-le avec précaution pour éviter une surcharge de notifications.");
		    add("Ce service est lié aux spécifications des déclencheurs. Utilisez-le avec prudence, car une mauvaise configuration peut entraîner des déclenchements inattendus ou des problèmes de performance.");
		    add("Pour obtenir des jetons d'accès OAuth. Essentiel pour l'authentification et l'autorisation.");
		    add("Pour vérifier l'état et les détails d'un jeton OAuth. Utilisé pour la gestion des jetons et la sécurité.");
		    add("Pour obtenir des jetons d'accès OAuth. Essentiel pour l'authentification et l'autorisation.");
		    add("Pour vérifier l'état et les détails d'un jeton OAuth. Utile pour la gestion des jetons et la sécurité.");
		}};




		List<String> servicesPrudence = new ArrayList<String>() {{
			add("pub.math:subtractInts");
		    add("pub.flow:clearPipeline");
		    add("pub.flow:setResponse");
		    add("pub.flow:setResponse2");
		    add("pub.flow:setResponseCode");
		    add("pub.flow:setResponseHeader");
		    add("pub.flow:setResponseHeaders");
		    add("pub.utils.messaging:syncDocTypesToUM");
		    add("pub.utils.ws:deepClone");
		    add("pub.tx:resetOutbound");
		    add("pub.tx:shutdown");
		    add("pub.synchronization.xref:deleteByObjectId");
		    add("pub.synchronization.xref:deleteXReference");
		    add("pub.string:substitutePipelineVariables");
		    add("pub.storage:closeStore");
		    add("pub.storage:deleteStore");
		    add("pub.storage:releaseLocks");
		    add("pub.storage:remove");
		    add("pub.soap.handler:closeStore");
		    add("pub.soap.handler:deleteStore");
		    add("pub.soap.handler:releaseLocks");
		    add("pub.soap.handler:remove");
		    add("pub.soap.handler:removeBodyBlock");
		    add("pub.soap.handler:removeHeaderBlock");
		    add("pub.soap.handler:removeProperty");
		    add("pub.soap.handler:unregisterConsumer");
		    add("pub.soap.handler:unregisterProvider");
		    add("pub.utils.messaging:syncDocTypesToUM");
		    add("pub.soap.handler:closeSequence");
		    add("pub.security.outboundPasswords:getPassword");
		    add("pub.security.outboundPasswords:listKeys");
		    add("pub.security.outboundPasswords:removePassword");
		    add("pub.security.outboundPasswords:setPassword");
		    add("pub.security.outboundPasswords:updatePassword");
		    add("pub.security:clearAuthenticationCache");
		    add("pub.security:decrypt");
		    add("pub.security:decryptAndVerify");
		    add("pub.security:encrypt");
		    add("pub.security:sign");
		    add("pub.security:signAndEncrypt");
		    add("pub.security:verify");
		    add("pub.replicator:generateReplicationEvent");
		    add("pub.replicator:notifyPackageRelease");
		    add("pub.mqtt:triggerSpec");
		    add("pub.oauth:token");
		    add("pub.oauth:introspectToken");
		    add("pub.oauth:token");
		    add("pub.oauth:introspectToken");
		}};
		
		List<String> servicesEviterExplications = new ArrayList<String>() {{
		    add("Utiliser avec parcimonie. Peut générer des volumes importants de journaux et affecter les performances.");
		    add("Devrait être évité en production car il expose les données du pipeline et peut générer de gros volumes de journaux.");
		    add("Éviter. Utile pour le débogage, mais peut introduire des données sensibles dans le pipeline.");
		    add("Éviter. Utilisé pour restaurer des états de pipeline à partir de fichiers, utile pour le débogage mais dangereux en production.");
		    add("Éviter. Sauvegarde l'état actuel du pipeline, ce qui peut inclure des données sensibles.");
		    add("Éviter. Similaire à savePipeline, mais sauvegarde dans un fichier, augmentant le risque de fuite de données.");
		    add("Risques de sécurité élevés.");
		    add("Opération de migration majeure.");
		    add("Peut causer des problèmes de compatibilité si mal géré.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services permettent de manipuler les entrées du message SOAP, mais leur utilisation directe peut rendre le traitement du message moins efficace et complexifier la maintenance.");
		    add("Ces services sont plus orientés vers la gestion des spécifications SOAP pour les services web. Leur utilisation peut rendre le système plus rigide et moins flexible.");
		    add("Ces services sont plus orientés vers la gestion des spécifications SOAP pour les services web. Leur utilisation peut rendre le système plus rigide et moins flexible.");
		    add("Ces services manipulent des erreurs SOAP et peuvent potentiellement introduire des problèmes de sécurité ou de compatibilité s'ils ne sont pas utilisés correctement.");
		    add("Ces services manipulent des erreurs SOAP et peuvent potentiellement introduire des problèmes de sécurité ou de compatibilité s'ils ne sont pas utilisés correctement.");
		    add("Cela semble être une méthode pour sortir d'une situation d'échec, mais son utilisation directe peut entraîner des comportements inattendus ou des interruptions dans le flux de traitement.");
		    add("Ce service arrête l'exécution du flux de service, il n'est pas recommandé en production car il peut interrompre brutalement le traitement sans gestion appropriée des erreurs.");
		    add("Ce service semble être un service de test ou de débogage, il ne devrait pas être utilisé en production.");
		}};


		List<String> servicesEviter = new ArrayList<String>() {{
		    add("pub.flow:debugLog");
		    add("pub.flow:tracePipeline");
		    add("pub.flow:restorePipeline");
		    add("pub.flow:restorePipelineFromFile");
		    add("pub.flow:savePipeline");
		    add("pub.flow:savePipelineToFile");
		    add("pub.utils.ws:executeOSCommand");
		    add("pub.utils.messaging:migrateDocTypesTriggersToUM");
		    add("pub.utils.ws:setCompatibilityModeFalse");
		    add("pub.soap:addBodyEntry");
		    add("pub.soap:addHeaderEntry");
		    add("pub.soap:addTrailer");
		    add("pub.soap:removeBodyEntry");
		    add("pub.soap:removeHeaderEntry");
		    add("pub.soap:removeTrailer");
		    add("pub.soap:callbackServiceSpec");
		    add("pub.soap:requestResponseSpec");
		    add("pub.soap:convertToVersionSpecificSOAPFault");
		    add("pub.soap:validateSoapData");
		    add("pub.utils:exitUnable");
		    add("pub.utils.ws.exitUnable");
		    add("pub.utils.ws.ToUnderstand");
		}};
		
        if (node.getNodeType() == Node.ELEMENT_NODE) {
        	if(node.getNodeName().equals("INVOKE")) {
        		NamedNodeMap map=node.getAttributes();
        		/*for(int i=0; i<map.getLength();i++) {
        			System.out.println(map.item(i).getNodeName());
        		}*/
        		Node node1=map.item(0);
    			//System.out.println(node1.getNodeValue());
    			String service=node1.getNodeValue();
    			//System.out.println(servicesPrudence.contains(service));
    			
    				if(servicesEviter.contains(service)) {
    					logger.severe("The Flow Servie "+fileName+" invoke the service "+service+" which should be avoided in prod due to: "+servicesEviterExplications.get(servicesEviter.indexOf(service)));
    					if(!Error) {
    						Error=true;
    					}
    				}
    				if(servicesPrudence.contains(service)) {
    					logger.warning("The Flow Servie "+fileName+" invoke the service "+service+" which should be used with high Prudence in prod due to "+servicesPrudenceExplications.get(servicesPrudence.indexOf(service)));
    				}
    			
    			
        		
        	}
            //System.out.println(node.getNodeName());
            NodeList children = node.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
            	traverseAndInvokedServices(children.item(i), fileName);
                //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
            }
        }
    }
	
	public static Boolean checkClearPiplineInvokedFirst(Node node) {
		
    if (node.getNodeType() == Node.ELEMENT_NODE) {
    	//System.out.println(node.getNodeName());
    
        //System.out.println(node.getNodeName());
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
        	//System.out.println(children.item(i).getNodeName());
        	if(!children.item(i).getNodeName().equals("#text")&&!children.item(i).getNodeName().equals("#comment")&&!children.item(i).getNodeName().equals("COMMENT")) {
        		if(!children.item(i).getNodeName().equals("INVOKE")) {
        			return false;
        		}
        		else {
        			NamedNodeMap map=children.item(i).getAttributes();
            		/*for(int i=0; i<map.getLength();i++) {
            			System.out.println(map.item(i).getNodeName());
            		}*/
            		Node node1=map.item(0);
        			//System.out.println(node1.getNodeValue());
        			String service=node1.getNodeValue();
        			if(!service.equals("pub.flow:clearPipeline")) return false;
        			
        		}
        		break;
        	
        	}
            //if(children.item(i).getNodeName().equals("Values")) System.out.println("-------------------------rdr-------------");
        }
    }
    return true;
}

}
