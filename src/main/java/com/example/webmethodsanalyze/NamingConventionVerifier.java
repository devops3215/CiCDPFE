package com.example.webmethodsanalyze;

import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class NamingConventionVerifier {
    private static final Set<String> EXCEPTIONS = new HashSet<>();

    static {
        EXCEPTIONS.add("SAP");
        EXCEPTIONS.add("ABAC");
        EXCEPTIONS.add("CNX");
    }

    public static String verifyPackageName(String packageName) {
        if (!packageName.matches("^[A-Z][a-zA-Z0-9]*$")) {

            return "Invalid package name: " + packageName + ". Suggested: " + suggestCamelCase(packageName, true);
        }
        return "Valid package name: " + packageName;
    }

    public static String verifyVariableName(String variableName) {
        return verifyCamelCaseVerb(variableName, false, "variable");
    }
    public static String verifyRootFolderName(String rootFolderName, String packageName) {
        if (!rootFolderName.equals(packageName) && !rootFolderName.matches("^[A-Z][a-zA-Z0-9]*$")) {
            return "Invalid root folder name: " + rootFolderName + ". Suggested: " + suggestCamelCase(packageName, true);
        }
        return verifyPackageName(rootFolderName);
    }

    public static String verifyOtherFolderName(String folderName) {
        if (!folderName.matches("^[a-z][a-z0-9]*$")) {
            return "Invalid folder name: " + folderName + ". Suggested: " + lowerCaseAndRemoveSpecialCharacters(folderName);
        }
        return "Valid folder name: " + folderName;
    }

    public static String verifyFlowServiceName(String serviceName) {
        return verifyCamelCaseVerb(serviceName, false, "flow service");
    }

    public static String verifyJavaServiceName(String serviceName) {
        return verifyCamelCaseVerb(serviceName, false, "java service");
    }

    public static String verifyConnectionName(String connectionName) {
        if (!connectionName.matches("^[A-Z][a-zA-Z0-9]*$")) {
            return "Invalid connection name: " + connectionName + ". Suggested: " + suggestCamelCase(connectionName, true);
        }
        return "Valid connection name: " + connectionName;
    }

    public static String verifyAdapterServiceName(String serviceName) {
        return verifyCamelCaseVerb(serviceName, false, "adapter service");
    }

    public static String verifyTriggerName(String triggerName) {
        return verifyCamelCaseVerb(triggerName, false, "trigger");
    }

    public static String verifyDocumentTypeName(String documentTypeName) {
        if (!documentTypeName.matches("^[A-Z][a-zA-Z0-9]*$")) {
            return "Invalid document type name: " + documentTypeName + ". Suggested: " + suggestCamelCase(documentTypeName, true);
        }
        return "Valid document type name: " + documentTypeName;
    }

    private static String verifyCamelCaseVerb(String name, boolean startsWithUppercase, String type) {
        if (!name.matches(startsWithUppercase ? "^[A-Z][a-zA-Z0-9]*$" : "^[a-z][a-zA-Z0-9]*$")) {
            return "Invalid " + type + " name: " + name + ". Suggested: " + suggestCamelCase(name, startsWithUppercase);
        } else if (!Character.isLowerCase(name.charAt(0))) {
            return "Invalid " + type + " name: " + name + ". Suggested: " + suggestCamelCase(name, false);
        }
        return "Valid " + type + " name: " + name;
    }

    public static String lowerCaseAndRemoveSpecialCharacters(String name) {
        return name.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
    }

    private static String suggestCamelCase(String name, boolean startsWithUppercase) {
        StringBuilder suggestedName = new StringBuilder();
        boolean nextUpper = startsWithUppercase;

        // Use a regex to split the input by exception words while keeping delimiters
        String regex = String.join("|", EXCEPTIONS);
        Pattern pattern = Pattern.compile("("+regex+")", Pattern.CASE_INSENSITIVE);
        String[] parts = pattern.split(name);
        Matcher matcher = pattern.matcher(name);

        int partIndex = 0;

        while (matcher.find()) {
            String beforeException = (partIndex < parts.length) ? parts[partIndex++] : "";
            String exception = matcher.group();

            // Apply camel case to the segment before the exception
            suggestedName.append(applyCamelCase(beforeException, nextUpper));

            // Append the exception word in uppercase
            suggestedName.append(exception);

            nextUpper = true; // Next segment starts with uppercase
        }

        // Handle the last part after the last exception (if any)
        if (partIndex < parts.length) {
            suggestedName.append(applyCamelCase(parts[partIndex], nextUpper));
        }

        return suggestedName.toString();
    }

    private static String applyCamelCase(String segment, boolean startWithUpperCase) {
        StringBuilder builder = new StringBuilder();
        boolean nextUpper = startWithUpperCase;

        for (char ch : segment.toCharArray()) {
            if (Character.isLetterOrDigit(ch)) {
                if (nextUpper) {
                    builder.append(Character.toUpperCase(ch));
                    nextUpper = false;
                } else {
                    builder.append(Character.toLowerCase(ch));
                }
            } else {
                nextUpper = true; // Non-alphanumeric characters reset to uppercase for the next word
            }
        }

        return builder.toString();
    }
    public static String validateNodeName(PackageNode node, String packageName) {
        switch (node.getType()) {
            case PACKAGE:
                return NamingConventionVerifier.verifyPackageName(node.getName());
            case FOLDER:
                if (node.getParent() != null && node.getParent().getType() == NodeType.PACKAGE) {
                    return NamingConventionVerifier.verifyRootFolderName(node.getName(), packageName);
                } else {
                    return NamingConventionVerifier.verifyOtherFolderName(node.getName());
                }
            case FLOW:
                return NamingConventionVerifier.verifyFlowServiceName(node.getName());
            case JAVA_SERVICE:
                return NamingConventionVerifier.verifyJavaServiceName(node.getName());
            case CONNECTION_DATA:
                return NamingConventionVerifier.verifyConnectionName(node.getName());
            case ADAPTER_SERVICE:
                return NamingConventionVerifier.verifyAdapterServiceName(node.getName());
            case TRIGGER:
                return NamingConventionVerifier.verifyTriggerName(node.getName());
            case DOCUMENT_TYPE:
                return NamingConventionVerifier.verifyDocumentTypeName(node.getName());
            default:
                return "Unknown node type";
        }
    }
}
