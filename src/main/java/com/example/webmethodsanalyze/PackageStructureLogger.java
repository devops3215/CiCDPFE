package com.example.webmethodsanalyze;

import java.util.logging.Logger;

public class PackageStructureLogger {

    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

    public static void  logStructure(PackageNode node, String type) {

        if(node.getType().toString().equals(type)){

            System.out.println("haja khra");
        String validationMessage = NamingConventionVerifier.validateNodeName(node, node.getParent() != null ? node.getParent().getName() : node.getName());
        if (validationMessage.startsWith("Invalid")) {
            logger.severe( validationMessage);
        }else if (validationMessage.startsWith("Valid")){
            logger.info( validationMessage);
        }
        }
        for (PackageNode child : node.getChildren()) {
            logStructure(child, type);
        }
    }
}
